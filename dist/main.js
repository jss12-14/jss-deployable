(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	var module = __webpack_require__(id);
	return module;
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-topbar *ngIf=\"isLoggedIn()\"></app-topbar>\r\n<app-login *ngIf=\"!isLoggedIn()\"></app-login>\r\n<router-outlet *ngIf=\"isLoggedIn()\"></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/app.component.less":
/*!************************************!*\
  !*** ./src/app/app.component.less ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./services/auth.service */ "./src/app/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(authService) {
        this.authService = authService;
    }
    AppComponent.prototype.isLoggedIn = function () {
        return this.authService.checkLoginStatus();
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.less */ "./src/app/app.component.less")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var angular2_ladda__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular2-ladda */ "./node_modules/angular2-ladda/module/module.js");
/* harmony import */ var _auth_interceptors_token_interceptor__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./auth/interceptors/token.interceptor */ "./src/app/auth/interceptors/token.interceptor.ts");
/* harmony import */ var _services_dimentional_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services/dimentional.service */ "./src/app/services/dimentional.service.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _new_report_new_report_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./new-report/new-report.component */ "./src/app/new-report/new-report.component.ts");
/* harmony import */ var _topbar_topbar_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./topbar/topbar.component */ "./src/app/topbar/topbar.component.ts");
/* harmony import */ var _modals_on_time_delivery_form_on_time_delivery_form_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./modals/on-time-delivery-form/on-time-delivery-form.component */ "./src/app/modals/on-time-delivery-form/on-time-delivery-form.component.ts");
/* harmony import */ var _modals_productivity_kpi_form_productivity_kpi_form_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./modals/productivity-kpi-form/productivity-kpi-form.component */ "./src/app/modals/productivity-kpi-form/productivity-kpi-form.component.ts");
/* harmony import */ var _modals_quality_form_quality_form_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./modals/quality-form/quality-form.component */ "./src/app/modals/quality-form/quality-form.component.ts");
/* harmony import */ var _modals_supplier_ppm_supplier_ppm_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./modals/supplier-ppm/supplier-ppm.component */ "./src/app/modals/supplier-ppm/supplier-ppm.component.ts");
/* harmony import */ var _modals_delete_confirmation_delete_confirmation_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./modals/delete-confirmation/delete-confirmation.component */ "./src/app/modals/delete-confirmation/delete-confirmation.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















var appRoutes = [
    { path: '', component: _home_home_component__WEBPACK_IMPORTED_MODULE_11__["HomeComponent"] },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_10__["LoginComponent"] },
    { path: 'records/:year/:month', component: _new_report_new_report_component__WEBPACK_IMPORTED_MODULE_12__["NewReportComponent"] }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_10__["LoginComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_11__["HomeComponent"],
                _new_report_new_report_component__WEBPACK_IMPORTED_MODULE_12__["NewReportComponent"],
                _topbar_topbar_component__WEBPACK_IMPORTED_MODULE_13__["TopbarComponent"],
                _modals_on_time_delivery_form_on_time_delivery_form_component__WEBPACK_IMPORTED_MODULE_14__["OnTimeDeliveryFormComponent"],
                _modals_productivity_kpi_form_productivity_kpi_form_component__WEBPACK_IMPORTED_MODULE_15__["ProductivityKpiFormComponent"],
                _modals_quality_form_quality_form_component__WEBPACK_IMPORTED_MODULE_16__["QualityFormComponent"],
                _modals_supplier_ppm_supplier_ppm_component__WEBPACK_IMPORTED_MODULE_17__["SupplierPpmComponent"],
                _modals_delete_confirmation_delete_confirmation_component__WEBPACK_IMPORTED_MODULE_18__["DeleteConfirmationComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(appRoutes),
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                angular2_ladda__WEBPACK_IMPORTED_MODULE_6__["LaddaModule"].forRoot({
                    style: "expand-right",
                    spinnerLines: 12
                })
            ],
            entryComponents: [
                _modals_on_time_delivery_form_on_time_delivery_form_component__WEBPACK_IMPORTED_MODULE_14__["OnTimeDeliveryFormComponent"],
                _modals_productivity_kpi_form_productivity_kpi_form_component__WEBPACK_IMPORTED_MODULE_15__["ProductivityKpiFormComponent"],
                _modals_quality_form_quality_form_component__WEBPACK_IMPORTED_MODULE_16__["QualityFormComponent"],
                _modals_supplier_ppm_supplier_ppm_component__WEBPACK_IMPORTED_MODULE_17__["SupplierPpmComponent"],
                _modals_delete_confirmation_delete_confirmation_component__WEBPACK_IMPORTED_MODULE_18__["DeleteConfirmationComponent"]
            ],
            providers: [
                _services_dimentional_service__WEBPACK_IMPORTED_MODULE_8__["DimentionalService"],
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"],
                    useClass: _auth_interceptors_token_interceptor__WEBPACK_IMPORTED_MODULE_7__["TokenInterceptor"],
                    multi: true
                }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth/interceptors/token.interceptor.ts":
/*!********************************************************!*\
  !*** ./src/app/auth/interceptors/token.interceptor.ts ***!
  \********************************************************/
/*! exports provided: TokenInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenInterceptor", function() { return TokenInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TokenInterceptor = /** @class */ (function () {
    function TokenInterceptor() {
    }
    TokenInterceptor.prototype.intercept = function (request, next) {
        if (request.url.includes('login')) {
            return next.handle(request);
        }
        else {
            var user = JSON.parse(localStorage.getItem('user'));
            request = request.clone({
                setHeaders: {
                    Authorization: "Bearer " + user.token
                }
            });
            return next.handle(request);
        }
    };
    TokenInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], TokenInterceptor);
    return TokenInterceptor;
}());



/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"home-wrapper\">\r\n  <div class=\"container\">\r\n    <div class=\"row mt-4\">\r\n      <div class=\"col-md\">\r\n        <h3>Add new report</h3>\r\n        <p>Choose year and month to add a new report</p>\r\n\r\n        <div class=\"year-row\">\r\n          <span class=\"year-entry h4 text-muted\"\r\n          [ngClass]=\"{'selectedYear': checkWhichYear(year)}\"\r\n          *ngFor=\"let year of calYears\"\r\n          (click)=\"displayReportsAvailability(selectedYear = year)\">\r\n          {{year}}</span>\r\n      </div>\r\n      <hr>\r\n      <div class=\"row\" *ngIf=\"calMonthsObj.length < 1\">\r\n        <div class=\"col-12 text-center h3 text-muted mt-4 mb-3\">\r\n          <span class=\"fa fa-fw fa-spin fa-spinner\"></span><span> Gathering monthly data. Please wait.</span>\r\n        </div>\r\n      </div>\r\n      <div class=\"row mt-4\" *ngIf=\"calMonthsObj.length > 1\">\r\n        <div class=\"col-md-2 col-4\" *ngFor=\"let calMonthObj of calMonthsObj\">\r\n          <div class=\"card mb-3\" [ngClass]=\"{'recordPresent' : calMonthObj.Present_Anywhere}\">\r\n            <div class=\"card-body text-center\">\r\n              <span class=\"h5\">{{calMonthObj.MonthName}}</span> <br>\r\n              <div *ngIf=\"calMonthObj.Present_Anywhere\">\r\n                <span class=\"text-success\">Report Present</span>\r\n                <div class=\"text-center mt-2\">\r\n                  <a class=\"btn btn-block btn-sm btn-outline-warning\" [routerLink]=\"['records', selectedYear, calMonthObj.MonthNumber]\"\r\n                     [queryParams]=\"{viewOnly: true}\" >Select</a>\r\n                </div>\r\n              </div>\r\n              <div *ngIf=\"!calMonthObj.Present_Anywhere\">\r\n                <span class=\"text-danger\">No Report</span>\r\n                <div class=\"text-center mt-2\" *ngIf=\"checkYrAndMonth(selectedYear,calMonthObj.MonthNumber)\">\r\n                  <button class=\"btn btn-block btn-sm btn-outline-primary\" [routerLink]=\"['records', selectedYear, calMonthObj.MonthNumber]\"\r\n                          (click)=\"setDate(selectedYear,calMonthObj.MonthNumber)\">Add New</button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"gutter separator\"></div>\r\n  <div class=\"text-center\">\r\n    <img src=\"../assets/img/logo-transparent.png\" class=\"img-fluid\" alt=\"Responsive image\" style=\"height:80px;\">\r\n  </div>\r\n  <!-- <div class=\"row records-history\">\r\n    <div class=\"col-md\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <h2 class=\"card-title\"><span class=\"card-title-text card-title-text-history pb-2\">Records History</span></h2>\r\n          <table class=\"table mt-5 records-history-table\">\r\n            <thead>\r\n              <tr>\r\n                <th scope=\"col\">#</th>\r\n                <th scope=\"col\">First</th>\r\n                <th scope=\"col\">Last</th>\r\n                <th scope=\"col\">Handle</th>\r\n                <th scope=\"col\">Actions</th>\r\n              </tr>\r\n            </thead>\r\n            <tbody>\r\n              <tr>\r\n                <th scope=\"row\">1</th>\r\n                <td>Mark</td>\r\n                <td>Otto</td>\r\n                <td>@mdo</td>\r\n                <td class=\"row-actions\">\r\n                  <span class=\"fa fw-fw fa-pencil\"></span>\r\n                  <span class=\"fa fw-fw fa-trash\"></span>\r\n                </td>\r\n              </tr>\r\n              <tr>\r\n                <th scope=\"row\">2</th>\r\n                <td>Jacob</td>\r\n                <td>Thornton</td>\r\n                <td>@fat</td>\r\n                <td class=\"row-actions\">\r\n                  <span class=\"fa fw-fw fa-pencil\"></span>\r\n                  <span class=\"fa fw-fw fa-trash\"></span>\r\n                </td>\r\n              </tr>\r\n              <tr>\r\n                <th scope=\"row\">3</th>\r\n                <td>Larry</td>\r\n                <td>the Bird</td>\r\n                <td>@twitter</td>\r\n                <td class=\"row-actions\">\r\n                  <span class=\"fa fw-fw fa-pencil\"></span>\r\n                  <span class=\"fa fw-fw fa-trash\"></span>\r\n                </td>\r\n              </tr>\r\n            </tbody>\r\n          </table>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div> -->\r\n</div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/home/home.component.less":
/*!******************************************!*\
  !*** ./src/app/home/home.component.less ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".year-row {\n  display: flex;\n  justify-content: space-between;\n  text-align: center;\n}\n.year-row .year-entry {\n  cursor: pointer;\n  padding: 5px 15px;\n}\n.year-row .selectedYear {\n  color: black !important;\n  font-size: 36px !important;\n}\n.gutter.separator {\n  border-top: 2px solid #333;\n  margin: 2rem 1rem 3rem 2rem;\n}\n.records-history-table .row-actions span {\n  padding: 0px 5px;\n}\n.recordPresent {\n  background-color: #F3F0F0;\n}\n.card .card-body .card-title-text-history {\n  border-bottom: 3px solid purple;\n}\n"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_dimentional_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/dimentional.service */ "./src/app/services/dimentional.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomeComponent = /** @class */ (function () {
    function HomeComponent(dimentionalService) {
        this.dimentionalService = dimentionalService;
        this.calYears = ['2017', '2018', '2019', '2020', '2021', '2022', '2023', '2024'];
        this.calMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        this.calMonthsObj = [];
        this.dimentionalService.resetFields();
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.selectedYear = new Date().getFullYear();
        this.setPlant();
    };
    HomeComponent.prototype.checkYearAndMonth = function (year, month) {
        if (month > new Date().getMonth() + 1) {
            return true;
        }
        else {
            if (this.selectedYear > new Date().getFullYear()) {
                return true;
            }
            else {
                return false;
            }
        }
    };
    HomeComponent.prototype.setDate = function (year, month) {
        this.dimentionalService.createdDate = year + '-' + (month < 10 ? '0' + month : month) + '-01';
    };
    HomeComponent.prototype.setPlant = function () {
        var _this = this;
        this.dimentionalService.getPlants().subscribe(function (result) {
            _this.plantObj = result;
            _this.dimentionalService.regionName = _this.plantObj[0].Region;
            _this.dimentionalService.plantName = _this.plantObj[0].Plant_Name;
            _this.dimentionalService.plantId = _this.plantObj[0].Plant_ID;
            localStorage.setItem('plantId', _this.plantObj[0].Plant_ID);
            _this.displayReportsAvailability();
        });
    };
    HomeComponent.prototype.displayReportsAvailability = function () {
        var _this = this;
        this.dimentionalService.getReportsPresentForYear(this.selectedYear, this.dimentionalService.plantId).subscribe(function (reports) {
            console.log('reports', reports);
            _this.calMonthsObj = reports;
        });
    };
    HomeComponent.prototype.checkYrAndMonth = function (year, month) {
        if (year < new Date().getFullYear()) {
            return true;
        }
        else if (year == new Date().getFullYear()) {
            if (month <= new Date().getMonth() + 1) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    };
    HomeComponent.prototype.checkWhichYear = function (year) {
        if (year == this.selectedYear) {
            return true;
        }
        else {
            return false;
        }
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.less */ "./src/app/home/home.component.less")]
        }),
        __metadata("design:paramtypes", [_services_dimentional_service__WEBPACK_IMPORTED_MODULE_1__["DimentionalService"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n  <div class=\"row justify-content-center\" style=\"margin-top:20%\">\r\n    <div class=\"col-sm-6\">\r\n      <div class=\"card\">\r\n        <div class=\"card-body\">\r\n          <div class=\"card-title text-center pt-3\">\r\n            <img src=\"../assets/img/logo-transparent.png\" alt=\"\" height=\"70px\">\r\n            <h2 class=\"mt-5\">Login</h2>\r\n          </div>\r\n          <div class=\"card-content mb-5\">\r\n            <div class=\"login-form pl-5 pr-5 pt-4\">\r\n              <div class=\"form-group\">\r\n                <form (keydown)=\"keyDownFunction($event)\">\r\n                  <input type=\"text\" class=\"form-control mb-3\" placeholder=\"username\" [(ngModel)]=\"username\" [ngModelOptions]=\"{standalone: true}\">\r\n                  <input type=\"password\" class=\"form-control\" placeholder=\"password\" [(ngModel)]=\"password\" [ngModelOptions]=\"{standalone: true}\">\r\n                </form>\r\n              </div>\r\n              <div class=\"failedLogin-prompt text-danger\">\r\n                <p *ngIf=\"!failedLogin\">&nbsp;</p>\r\n                <p *ngIf=\"failedLogin\">The username or password is incorrect. Please try again.</p>\r\n              </div>\r\n              <div class=\"action pl-5 pr-5 pt-2\">\r\n                <button class=\"button btn btn-primary btn-block\" (click)=\"attemptLogin()\" [ladda]=\"attemptingLogin\" (keydown.enter)=\"test()\">\r\n                  Login\r\n                </button>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/login/login.component.less":
/*!********************************************!*\
  !*** ./src/app/login/login.component.less ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginComponent = /** @class */ (function () {
    function LoginComponent(authService, router) {
        this.authService = authService;
        this.router = router;
        this.username = null;
        this.password = null;
        this.attemptingLogin = false;
        this.failedLogin = false;
        this.authService.logout();
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.attemptLogin = function () {
        var _this = this;
        this.attemptingLogin = true;
        this.authService.login(this.username, this.password).subscribe(function (returnPayload) {
            //console.log("returnPayload", returnPayload);
            _this.attemptingLogin = false;
            localStorage.setItem('user', JSON.stringify(returnPayload));
            _this.router.navigate(['']);
        }, function (err) {
            _this.attemptingLogin = false;
            _this.failedLogin = true;
            _this.username = null;
            _this.password = null;
            setTimeout(function () {
                _this.failedLogin = false;
            }, 5000);
        });
    };
    LoginComponent.prototype.keyDownFunction = function (event) {
        if (event.keyCode == 13) {
            this.attemptLogin();
        }
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.less */ "./src/app/login/login.component.less")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/modals/delete-confirmation/delete-confirmation.component.html":
/*!*******************************************************************************!*\
  !*** ./src/app/modals/delete-confirmation/delete-confirmation.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"card-body text-center\">\r\n    <h3>This will permanantly delete this report. <br> Are you sure you want to proceed?</h3>\r\n  </div>\r\n  <div class=\"card-footer text-right\">\r\n    <button class=\"btn btn-danger mr-2\" (click)=\"confirmDelete()\" [ladda]=\"deleting\"><span class=\"fa fa-fw fa-trash\"></span> Delete</button>\r\n    <button class=\"btn btn-secondary\" (click)=\"activeModal.dismiss('Cross click')\"><span class=\"fa fa-fw fa-times\"></span> Cancel</button>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modals/delete-confirmation/delete-confirmation.component.less":
/*!*******************************************************************************!*\
  !*** ./src/app/modals/delete-confirmation/delete-confirmation.component.less ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/modals/delete-confirmation/delete-confirmation.component.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/modals/delete-confirmation/delete-confirmation.component.ts ***!
  \*****************************************************************************/
/*! exports provided: DeleteConfirmationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteConfirmationComponent", function() { return DeleteConfirmationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _services_dimentional_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/dimentional.service */ "./src/app/services/dimentional.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DeleteConfirmationComponent = /** @class */ (function () {
    function DeleteConfirmationComponent(activatedRoute, router, dimentionalService, activeModal) {
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.dimentionalService = dimentionalService;
        this.activeModal = activeModal;
        this.deleting = false;
    }
    DeleteConfirmationComponent.prototype.ngOnInit = function () {
    };
    DeleteConfirmationComponent.prototype.confirmDelete = function () {
        var _this = this;
        this.deleting = true;
        var date = new Date(this.date).toISOString();
        var plantId = localStorage.getItem('plantId');
        console.log(plantId);
        this.dimentionalService.deleteRecord(date.substring(0, 10), plantId).subscribe(function (result) {
            console.log(result);
            _this.deleting = false;
            _this.activeModal.dismiss('Timeout');
            _this.router.navigate(['']);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], DeleteConfirmationComponent.prototype, "date", void 0);
    DeleteConfirmationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-delete-confirmation',
            template: __webpack_require__(/*! ./delete-confirmation.component.html */ "./src/app/modals/delete-confirmation/delete-confirmation.component.html"),
            styles: [__webpack_require__(/*! ./delete-confirmation.component.less */ "./src/app/modals/delete-confirmation/delete-confirmation.component.less")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _services_dimentional_service__WEBPACK_IMPORTED_MODULE_3__["DimentionalService"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbActiveModal"]])
    ], DeleteConfirmationComponent);
    return DeleteConfirmationComponent;
}());



/***/ }),

/***/ "./src/app/modals/on-time-delivery-form/on-time-delivery-form.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/modals/on-time-delivery-form/on-time-delivery-form.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"card-header\">\r\n    <h4>On Time Delivery - Add Entry</h4>\r\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss('Cross click')\" style=\"position: absolute;\r\n    top: 3px;\r\n    right: 10px;\">\r\n      <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n  </div>\r\n  <div class=\"card-body\">\r\n    <div class=\"on-time-form\">\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-7 col-form-label\">Product<font>*</font></label>\r\n        <div class=\"col-sm-5\">\r\n          <input\r\n            id=\"quality-productName\"\r\n            type=\"text\"\r\n            name=\"product\"\r\n            class=\"form-control\"\r\n            [(ngModel)]=\"onTimeDeliveryTablePayload.Prod_ID\"\r\n            [ngbTypeahead]=\"searchProducts\"\r\n            (focus)=\"focusProduct$.next($event.target.value)\"\r\n            (click)=\"clickProduct$.next($event.target.value)\"\r\n            #instance=\"ngbTypeahead\"\r\n            [ngModelOptions]=\"{standalone: true}\"\r\n            #pname=\"ngModel\" required/>\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"pname.invalid && (pname.dirty || pname.touched)\">Product Name is\r\n          required\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-7 col-form-label\">Customer<font>*</font></label>\r\n        <div class=\"col-sm-5\">\r\n          <input\r\n            id=\"quality-customerName\"\r\n            type=\"text\"\r\n            name=\"customer\"\r\n            class=\"form-control\"\r\n            [(ngModel)]=\"onTimeDeliveryTablePayload.Cust_ID\"\r\n            [ngbTypeahead]=\"searchCustomers\"\r\n            (focus)=\"focusCustomer$.next($event.target.value)\"\r\n            (click)=\"clickCustomer$.next($event.target.value)\"\r\n            #instance=\"ngbTypeahead\"\r\n            [ngModelOptions]=\"{standalone: true}\"\r\n            #cname=\"ngModel\" required/>\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"cname.invalid && (cname.dirty || cname.touched)\">Customer Name is\r\n          required\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-7 col-form-label\">Qty of conforming products shipped for the\r\n          month<font>*</font></label>\r\n        <div class=\"col-sm-5\">\r\n          <input type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"onTimeDeliveryTablePayload.Cnf_Prod_Shipped_Mon\" #qty=\"ngModel\" required>\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"qty.invalid && (qty.dirty || qty.touched)\">Qty of conforming is\r\n          required\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-7 col-form-label\">Total Qty of Product Shipped for the\r\n          month<font>*</font></label>\r\n        <div class=\"col-sm-5\">\r\n          <input type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"onTimeDeliveryTablePayload.Tot_Prod_Shipped_Mon\" #totqty=\"ngModel\" required>\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"totqty.invalid && (totqty.dirty || totqty.touched)\">Total Qty of\r\n          Product Shipped is required\r\n        </div>\r\n      </div>\r\n\r\n    </div>\r\n  </div>\r\n  <div class=\"card-footer\">\r\n    <button class=\"btn btn-success btn-block mt-2 mb-2\"\r\n            *ngIf=\"cname.valid && pname.valid && qty.valid  && totqty.valid \" (click)=\"addOnTimeRecord()\">\r\n      <span class=\"fa fa-fw fa-save\"></span> Save\r\n    </button>\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modals/on-time-delivery-form/on-time-delivery-form.component.less":
/*!***********************************************************************************!*\
  !*** ./src/app/modals/on-time-delivery-form/on-time-delivery-form.component.less ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "font {\n  color: red;\n}\n#sample {\n  color: red;\n}\ninput.submitted.ng-invalid {\n  border: 1px solid black;\n}\n:-moz-ui-invalid:not(output) {\n  box-shadow: none;\n}\n"

/***/ }),

/***/ "./src/app/modals/on-time-delivery-form/on-time-delivery-form.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/modals/on-time-delivery-form/on-time-delivery-form.component.ts ***!
  \*********************************************************************************/
/*! exports provided: OnTimeDeliveryFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OnTimeDeliveryFormComponent", function() { return OnTimeDeliveryFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_dimentional_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/dimentional.service */ "./src/app/services/dimentional.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var OnTimeDeliveryFormComponent = /** @class */ (function () {
    function OnTimeDeliveryFormComponent(activeModal, dimentionalService) {
        var _this = this;
        this.activeModal = activeModal;
        this.dimentionalService = dimentionalService;
        this.onTimeDeliveryTablePayload = {
            Plant_ID: null,
            Benchmark_ID: null,
            Prod_ID: null,
            Sub_Prod_ID: null,
            Cust_ID: null,
            User_ID: null,
            Cnf_Prod_Shipped_Mon: null,
            Tot_Prod_Shipped_Mon: null,
            Revenue_Mon: null,
            Tot_Rjctd_Qty_For_Mon: null,
            Shipped_Qty_For_Mon: null,
            Scrap_For_Mon: null,
            No_Of_Complaints: null,
            Created_Date: null,
            Updated_Date: null
        };
        this.focusProduct$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.clickProduct$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.focusCustomer$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.clickCustomer$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.searchProducts = function (text$) {
            var debouncedText$ = text$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["debounceTime"])(200), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["distinctUntilChanged"])());
            var clicksWithClosedPopup$ = _this.clickProduct$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["filter"])(function () { return !_this.instance.isPopupOpen(); }));
            var inputFocus$ = _this.focusProduct$;
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["merge"])(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (term) { return (term === '' ? _this.dimentionalService.products
                : _this.dimentionalService.products.filter(function (v) { return v.toLowerCase().indexOf(term.toLowerCase()) > -1; })).slice(0, 10); }));
        };
        this.searchCustomers = function (text$) {
            var debouncedText$ = text$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["debounceTime"])(200), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["distinctUntilChanged"])());
            var clicksWithClosedPopup$ = _this.clickCustomer$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["filter"])(function () { return !_this.instance.isPopupOpen(); }));
            var inputFocus$ = _this.focusCustomer$;
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["merge"])(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (term) { return (term === '' ? _this.dimentionalService.customers
                : _this.dimentionalService.customers.filter(function (v) { return v.toLowerCase().indexOf(term.toLowerCase()) > -1; })).slice(0, 10); }));
        };
    }
    OnTimeDeliveryFormComponent.prototype.ngOnInit = function () {
    };
    OnTimeDeliveryFormComponent.prototype.addOnTimeRecord = function () {
        var _this = this;
        this.onTimeDeliveryTablePayload.Plant_ID = localStorage.getItem('plantId');
        this.onTimeDeliveryTablePayload.Benchmark_ID = this.dimentionalService.benchArray['Productivity'];
        this.onTimeDeliveryTablePayload.Prod_ID = this.dimentionalService.productLookupForID[this.onTimeDeliveryTablePayload.Prod_ID];
        this.onTimeDeliveryTablePayload.Cust_ID = this.dimentionalService.customerLookupForID[this.onTimeDeliveryTablePayload.Cust_ID];
        this.onTimeDeliveryTablePayload.Sub_Prod_ID = 'PRD000'; // no category
        this.onTimeDeliveryTablePayload.User_ID = JSON.parse(localStorage.getItem('user')).User_ID;
        this.onTimeDeliveryTablePayload.Created_Date = this.dimentionalService.createdDate;
        // this.onTimeDeliveryTablePayload.Updated_Date = moment().format("YYYY\-MM\-DD");
        this.dimentionalService.onTimeDeliveryData.push(this.onTimeDeliveryTablePayload);
        setTimeout(function () {
            _this.activeModal.dismiss('Timeout');
        }, 1000);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('instance'),
        __metadata("design:type", _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbTypeahead"])
    ], OnTimeDeliveryFormComponent.prototype, "instance", void 0);
    OnTimeDeliveryFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-on-time-delivery-form',
            template: __webpack_require__(/*! ./on-time-delivery-form.component.html */ "./src/app/modals/on-time-delivery-form/on-time-delivery-form.component.html"),
            styles: [__webpack_require__(/*! ./on-time-delivery-form.component.less */ "./src/app/modals/on-time-delivery-form/on-time-delivery-form.component.less")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"], _services_dimentional_service__WEBPACK_IMPORTED_MODULE_4__["DimentionalService"]])
    ], OnTimeDeliveryFormComponent);
    return OnTimeDeliveryFormComponent;
}());



/***/ }),

/***/ "./src/app/modals/productivity-kpi-form/productivity-kpi-form.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/modals/productivity-kpi-form/productivity-kpi-form.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"card-header\">\r\n    <h4>Productivity KPI - Add Entry</h4>\r\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss('Cross click')\" style=\"position: absolute;\r\n    top: 3px;\r\n    right: 10px;\">\r\n      <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n  </div>\r\n  <div class=\"card-body\">\r\n    <div class=\"productivity-kpi-form\">\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-9 col-form-label\">Product<font>*</font></label>\r\n        <div class=\"col-sm-3\">\r\n          <input id=\"quality-productName\" type=\"text\" name=\"product\" class=\"form-control\"\r\n                 [(ngModel)]=\"productivityKpiPayload.Prod_ID\"\r\n                 [ngbTypeahead]=\"searchProducts\" (focus)=\"focusProduct$.next($event.target.value)\"\r\n                 (click)=\"clickProduct$.next($event.target.value)\"\r\n                 #instance=\"ngbTypeahead\" [ngModelOptions]=\"{standalone: true}\" #pname=\"ngModel\" required/>\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"pname.invalid && (pname.dirty || pname.touched)\">Product Name is\r\n          required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-9 col-form-label\">Total Employees on first day of the\r\n          month<font>*</font></label>\r\n        <div class=\"col-sm-3\">\r\n          <input type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"productivityKpiPayload.Tot_Emp_First_Day_Mon\" #totemp=\"ngModel\" required>\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"totemp.invalid && (totemp.dirty || totemp.touched)\">Total Employees is\r\n          required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-9 col-form-label\">Total Employees on last day the\r\n          month<font>*</font></label>\r\n        <div class=\"col-sm-3\">\r\n          <input type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"productivityKpiPayload.Tot_Emp_Last_Day_Mon\" #totemplast=\"ngModel\" required>\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"totemplast.invalid && (totemplast.dirty || totemplast.touched)\">Total\r\n          Employees on last\r\n          day is required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-9 col-form-label\">Number of employees who left during the\r\n          Month<font>*</font></label>\r\n        <div class=\"col-sm-3\">\r\n          <input type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"productivityKpiPayload.Emp_Left_Mon\" #numofemp=\"ngModel\" required>\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"numofemp.invalid && (numofemp.dirty || numofemp.touched)\">Number of\r\n          employees who left is\r\n          required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-9 col-form-label\">$ Cost of premium freight In<font>*</font></label>\r\n        <div class=\"col-sm-3\">\r\n          <input type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"productivityKpiPayload.Cost_Premium_Freight_In_Mon\" #costofprein=\"ngModel\" required>\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"costofprein.invalid && (costofprein.dirty || costofprein.touched)\">Cost\r\n          of premium freight\r\n          In is required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-9 col-form-label\">$ Cost of premium freight\r\n          Out<font>*</font></label>\r\n        <div class=\"col-sm-3\">\r\n          <input type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"productivityKpiPayload.Cost_Premium_Freight_Out_Mon\" #costofpreout=\"ngModel\" required>\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"costofpreout.invalid && (costofpreout.dirty || costofpreout.touched)\">\r\n          Cost of premium\r\n          freight Out is required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-9 col-form-label\">$ Revenue for the month<font>*</font></label>\r\n        <div class=\"col-sm-3\">\r\n          <input type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"productivityKpiPayload.Revenue_Mon\" #revenue=\"ngModel\" required>\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"revenue.invalid && (revenue.dirty || revenue.touched)\">Revenue is\r\n          required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-9 col-form-label\">COGS<font>*</font></label>\r\n        <div class=\"col-sm-3\">\r\n          <input type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"productivityKpiPayload.Cogs\" #cogs=\"ngModel\" required>\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"cogs.invalid && (cogs.dirty || cogs.touched)\">COGS is required</div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-9 col-form-label\">Month end inventory value in local\r\n          currency<font>*</font></label>\r\n        <div class=\"col-sm-3\">\r\n          <input type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"productivityKpiPayload.Mon_End_Inv_Local_Curr\" #inventoryval=\"ngModel\" required>\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"inventoryval.invalid && (inventoryval.dirty || inventoryval.touched)\">\r\n          Month end inventory value is\r\n          required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-9 col-form-label\">Previous Month-End inventory value in local\r\n          currency<font>*</font></label>\r\n        <div class=\"col-sm-3\">\r\n          <input type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"productivityKpiPayload.Pre_Mon_End_Inv_Local_Curr\" #preinventoryval=\"ngModel\" required>\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\"\r\n             *ngIf=\"preinventoryval.invalid && (preinventoryval.dirty || preinventoryval.touched)\">\r\n          Previous Month-End inventory value is required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-9 col-form-label\">MFG Costs from P&L<font>*</font></label>\r\n        <div class=\"col-sm-3\">\r\n          <input type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"productivityKpiPayload.All_Mfg_Cost_PnL\" #mfgcost=\"ngModel\" required>\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"mfgcost.invalid && (mfgcost.dirty || mfgcost.touched)\">MFG Costs is\r\n          required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-9 col-form-label\">Overtime Costs from P&L<font>*</font></label>\r\n        <div class=\"col-sm-3\">\r\n          <input type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"productivityKpiPayload.Overtime_Cost_PnL\" #ovrcost=\"ngModel\" required>\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"ovrcost.invalid && (ovrcost.dirty || ovrcost.touched)\">Overtime Costs\r\n          is required\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"card-footer\">\r\n    <button class=\"btn btn-success btn-block mt-2 mb-2\"\r\n            *ngIf=\"pname.valid && totemp.valid && totemplast.valid  && numofemp.valid && costofprein.valid && costofpreout.valid && revenue.valid && cogs.valid && inventoryval.valid && preinventoryval.valid && mfgcost.valid && ovrcost.valid \"\r\n            (click)=\"addProductivityKPIRecord()\">\r\n      <span class=\"fa fa-fw fa-save\"></span> Save\r\n    </button>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modals/productivity-kpi-form/productivity-kpi-form.component.less":
/*!***********************************************************************************!*\
  !*** ./src/app/modals/productivity-kpi-form/productivity-kpi-form.component.less ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "font {\n  color: red;\n}\n#sample {\n  color: red;\n}\ninput.submitted.ng-invalid {\n  border: 1px solid black;\n}\n:-moz-ui-invalid:not(output) {\n  box-shadow: none;\n}\n"

/***/ }),

/***/ "./src/app/modals/productivity-kpi-form/productivity-kpi-form.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/modals/productivity-kpi-form/productivity-kpi-form.component.ts ***!
  \*********************************************************************************/
/*! exports provided: ProductivityKpiFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductivityKpiFormComponent", function() { return ProductivityKpiFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_dimentional_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/dimentional.service */ "./src/app/services/dimentional.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProductivityKpiFormComponent = /** @class */ (function () {
    function ProductivityKpiFormComponent(activeModal, dimentionalService) {
        var _this = this;
        this.activeModal = activeModal;
        this.dimentionalService = dimentionalService;
        this.productivityKpiPayload = {
            Plant_ID: null,
            Benchmark_ID: null,
            User_ID: null,
            Prod_ID: null,
            Emp_Left_Mon: null,
            Tot_Emp_First_Day_Mon: null,
            Tot_Emp_Last_Day_Mon: null,
            Tot_Supp_PPM_Qlty: null,
            Cost_Premium_Freight_In_Mon: null,
            Cost_Premium_Freight_Out_Mon: null,
            Revenue_Mon: null,
            Cogs: null,
            Mon_End_Inv_Local_Curr: null,
            Pre_Mon_End_Inv_Local_Curr: null,
            All_Mfg_Cost_PnL: null,
            Overtime_Cost_PnL: null,
            Created_Date: null,
            Updated_Date: null
            /* product: null,
            totEmpFirstDayMonth: null, Tot_Emp_First_Day_Mon
            totEmpLastDayMonth: null, Tot_Emp_Last_Day_Mon
            empLeftinMonth: null, Emp_Left_Mon
            premFrightinCostDollars: null, Cost_Premium_Freight_In_Mon
            premFrightoutCostDollars: null, Cost_Premium_Freight_Out_Mon
            revenueValue: null, Revenue_Mon
            cogs: null, Cogs
            monthEndInvValue: null, Mon_End_Inv_Local_Curr
            prevMonthEndInvValue: null, Pre_Mon_End_Inv_Local_Curr
            mfgCostsValue: null  All_Mfg_Cost_PnL*/
        };
        this.focusProduct$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.clickProduct$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.searchProducts = function (text$) {
            var debouncedText$ = text$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["debounceTime"])(200), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["distinctUntilChanged"])());
            var clicksWithClosedPopup$ = _this.clickProduct$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["filter"])(function () { return !_this.instance.isPopupOpen(); }));
            var inputFocus$ = _this.focusProduct$;
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["merge"])(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (term) { return (term === '' ? _this.dimentionalService.products
                : _this.dimentionalService.products.filter(function (v) { return v.toLowerCase().indexOf(term.toLowerCase()) > -1; })).slice(0, 10); }));
        };
    }
    ProductivityKpiFormComponent.prototype.ngOnInit = function () {
    };
    ProductivityKpiFormComponent.prototype.addProductivityKPIRecord = function () {
        var _this = this;
        this.productivityKpiPayload.Plant_ID = localStorage.getItem('plantId');
        this.productivityKpiPayload.Benchmark_ID = this.dimentionalService.benchArray['Productivity'];
        this.productivityKpiPayload.Prod_ID = this.dimentionalService.productLookupForID[this.productivityKpiPayload.Prod_ID];
        this.productivityKpiPayload.User_ID = JSON.parse(localStorage.getItem('user')).User_ID;
        this.productivityKpiPayload.Created_Date = this.dimentionalService.createdDate;
        // this.productivityKpiPayload.Updated_Date = moment().format("YYYY\-MM\-DD");
        this.dimentionalService.productivityData.push(this.productivityKpiPayload);
        setTimeout(function () {
            _this.activeModal.dismiss('Timeout');
        }, 1000);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('instance'),
        __metadata("design:type", _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbTypeahead"])
    ], ProductivityKpiFormComponent.prototype, "instance", void 0);
    ProductivityKpiFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-productivity-kpi-form',
            template: __webpack_require__(/*! ./productivity-kpi-form.component.html */ "./src/app/modals/productivity-kpi-form/productivity-kpi-form.component.html"),
            styles: [__webpack_require__(/*! ./productivity-kpi-form.component.less */ "./src/app/modals/productivity-kpi-form/productivity-kpi-form.component.less")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"], _services_dimentional_service__WEBPACK_IMPORTED_MODULE_4__["DimentionalService"]])
    ], ProductivityKpiFormComponent);
    return ProductivityKpiFormComponent;
}());



/***/ }),

/***/ "./src/app/modals/quality-form/quality-form.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/modals/quality-form/quality-form.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"card-header\">\r\n    <h4>Quality Record - Add Entry</h4>\r\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss('Cross click')\" style=\"position: absolute;\r\n    top: 3px;\r\n    right: 10px;\">\r\n      <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n  </div>\r\n  <div class=\"card-body\">\r\n    <div class=\"customer-form\">\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-7 col-form-label\">Customer<font>*</font></label>\r\n        <div class=\"col-sm-5\">\r\n          <input\r\n            #cname=\"ngModel\" required\r\n            id=\"quality-customerName\"\r\n            type=\"text\"\r\n            name=\"customer\"\r\n            class=\"form-control\"\r\n            [(ngModel)]=\"qualityTablePayload.Cust_ID\"\r\n            [ngbTypeahead]=\"searchCustomers\"\r\n            (focus)=\"focus$.next($event.target.value)\"\r\n            (click)=\"click$.next($event.target.value)\"\r\n            #instance=\"ngbTypeahead\"\r\n            [ngModelOptions]=\"{standalone: true}\"\r\n          />\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"cname.invalid && (cname.dirty || cname.touched)\">Customer Name is required</div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-7 col-form-label\">Product<font>*</font></label>\r\n        <div class=\"col-sm-5\">\r\n          <input\r\n            #pname=\"ngModel\" required\r\n            id=\"quality-productName\"\r\n            type=\"text\"\r\n            name=\"product\"\r\n            class=\"form-control\"\r\n            [(ngModel)]=\"qualityTablePayload.Prod_ID\"\r\n            [ngbTypeahead]=\"searchProducts\"\r\n            (focus)=\"focusProduct$.next($event.target.value)\"\r\n            (click)=\"clickProduct$.next($event.target.value)\"\r\n            #instance=\"ngbTypeahead\"\r\n            [ngModelOptions]=\"{standalone: true}\"\r\n          />\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"pname.invalid && (pname.dirty || pname.touched)\">Product Name is\r\n          required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-7 col-form-label\">Sub Product<font>*</font></label>\r\n        <div class=\"col-sm-5\">\r\n          <input\r\n            #spname=\"ngModel\" required\r\n            id=\"quality-subProductName\"\r\n            type=\"text\"\r\n            name=\"subProduct\"\r\n            class=\"form-control\"\r\n            [(ngModel)]=\"qualityTablePayload.Sub_Prod_ID\"\r\n            [ngbTypeahead]=\"searchSubProducts\"\r\n            (focus)=\"focusSubProduct$.next($event.target.value)\"\r\n            (click)=\"clickSubProduct$.next($event.target.value)\"\r\n            #instance=\"ngbTypeahead\"\r\n            [ngModelOptions]=\"{standalone: true}\"\r\n          />\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"spname.invalid && (spname.dirty || spname.touched)\">Sub Product is\r\n          required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-7 col-form-label\">Total Rejected Qty for the\r\n          month<font>*</font></label>\r\n        <div class=\"col-sm-5\">\r\n          <input #trq=\"ngModel\" required type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"qualityTablePayload.Tot_Rjctd_Qty_For_Mon\">\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"trq.invalid && (trq.dirty || trq.touched)\">Total Rejected Qty is\r\n          required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-7 col-form-label\">Shipped Quantity for the\r\n          month<font>*</font></label>\r\n        <div class=\"col-sm-5\">\r\n          <input #sq=\"ngModel\" required type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"qualityTablePayload.Shipped_Qty_For_Mon\">\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"sq.invalid && (sq.dirty || sq.touched)\">Shipped Quantity is required</div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-7 col-form-label\">$ Production Scrap for the\r\n          month<font>*</font></label>\r\n        <div class=\"col-sm-5\">\r\n          <input #ps=\"ngModel\" required type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"qualityTablePayload.Scrap_For_Mon\">\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"ps.invalid && (ps.dirty || ps.touched)\">Production Scrap is required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-7 col-form-label\">$ Revenue for the month<font>*</font></label>\r\n        <div class=\"col-sm-5\">\r\n          <input #revenue=\"ngModel\" required type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"qualityTablePayload.Revenue_Mon\">\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"revenue.invalid && (revenue.dirty || revenue.touched)\">Revenue is\r\n          required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-7 col-form-label\"># of Complaints<font>*</font></label>\r\n        <div class=\"col-sm-5\">\r\n          <input #complaints=\"ngModel\" required type=\"number\" class=\"form-control\" id=\"lostTimeIncedent\" value=\"5\"\r\n                 [(ngModel)]=\"qualityTablePayload.No_Of_Complaints\">\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"complaints.invalid && (complaints.dirty || complaints.touched)\">\r\n          # of Complaints is required\r\n        </div>\r\n      </div>\r\n\r\n    </div>\r\n\r\n  </div>\r\n  <div class=\"card-footer\">\r\n    <button class=\"btn btn-success btn-block mt-2 mb-2\"\r\n            *ngIf=\"cname.valid && pname.valid && spname.valid  && trq.valid && sq.valid && ps.valid && revenue.valid && complaints.valid \"\r\n            (click)=\"addQualityRecord()\" [ladda]=\"saving\">\r\n      <span class=\"fa fa-fw fa-save\"></span> Save\r\n    </button>\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modals/quality-form/quality-form.component.less":
/*!*****************************************************************!*\
  !*** ./src/app/modals/quality-form/quality-form.component.less ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "font {\n  color: red;\n}\n#sample {\n  color: red;\n}\ninput.submitted.ng-invalid {\n  border: 1px solid black;\n}\n:-moz-ui-invalid:not(output) {\n  box-shadow: none;\n}\n"

/***/ }),

/***/ "./src/app/modals/quality-form/quality-form.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/modals/quality-form/quality-form.component.ts ***!
  \***************************************************************/
/*! exports provided: QualityFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QualityFormComponent", function() { return QualityFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_dimentional_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/dimentional.service */ "./src/app/services/dimentional.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var QualityFormComponent = /** @class */ (function () {
    function QualityFormComponent(activeModal, dimentionalService) {
        var _this = this;
        this.activeModal = activeModal;
        this.dimentionalService = dimentionalService;
        this.saving = false;
        this.qualityTablePayload = {
            Plant_ID: null,
            Benchmark_ID: null,
            Prod_ID: null,
            Sub_Prod_ID: null,
            Cust_ID: null,
            User_ID: null,
            Cnf_Prod_Shipped_Mon: null,
            Tot_Prod_Shipped_Mon: null,
            Revenue_Mon: null,
            Tot_Rjctd_Qty_For_Mon: null,
            Shipped_Qty_For_Mon: null,
            Scrap_For_Mon: null,
            No_Of_Complaints: null,
            Created_Date: null,
            Updated_Date: null
        };
        this.focus$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.click$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.focusProduct$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.clickProduct$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.focusSubProduct$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.clickSubProduct$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.searchCustomers = function (text$) {
            var debouncedText$ = text$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["debounceTime"])(200), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["distinctUntilChanged"])());
            var clicksWithClosedPopup$ = _this.click$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["filter"])(function () { return !_this.instance.isPopupOpen(); }));
            var inputFocus$ = _this.focus$;
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["merge"])(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (term) { return (term === '' ? _this.dimentionalService.customers
                : _this.dimentionalService.customers.filter(function (v) { return v.toLowerCase().indexOf(term.toLowerCase()) > -1; })).slice(0, 10); }));
        };
        this.searchProducts = function (text$) {
            var debouncedText$ = text$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["debounceTime"])(200), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["distinctUntilChanged"])());
            var clicksWithClosedPopup$ = _this.clickProduct$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["filter"])(function () { return !_this.instance.isPopupOpen(); }));
            var inputFocus$ = _this.focusProduct$;
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["merge"])(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (term) { return (term === '' ? _this.dimentionalService.products
                : _this.dimentionalService.products.filter(function (v) { return v.toLowerCase().indexOf(term.toLowerCase()) > -1; })).slice(0, 10); }));
        };
        this.searchSubProducts = function (text$) {
            var debouncedText$ = text$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["debounceTime"])(200), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["distinctUntilChanged"])());
            var clicksWithClosedPopup$ = _this.clickSubProduct$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["filter"])(function () { return !_this.instance.isPopupOpen(); }));
            var inputFocus$ = _this.focusSubProduct$;
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["merge"])(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (term) { return (term === '' ? _this.dimentionalService.subProducts
                : _this.dimentionalService.subProducts.filter(function (v) { return v.toLowerCase().indexOf(term.toLowerCase()) > -1; })).slice(0, 10); }));
        };
    }
    QualityFormComponent.prototype.ngOnInit = function () {
    };
    QualityFormComponent.prototype.addQualityRecord = function () {
        var _this = this;
        this.saving = true;
        this.qualityTablePayload.Plant_ID = localStorage.getItem('plantId');
        this.qualityTablePayload.Benchmark_ID = this.dimentionalService.benchArray['Quality'];
        this.qualityTablePayload.Prod_ID = this.dimentionalService.productLookupForID[this.qualityTablePayload.Prod_ID];
        this.qualityTablePayload.Sub_Prod_ID = this.dimentionalService.subProductLookupForID[this.qualityTablePayload.Sub_Prod_ID];
        this.qualityTablePayload.Cust_ID = this.dimentionalService.customerLookupForID[this.qualityTablePayload.Cust_ID];
        this.qualityTablePayload.User_ID = JSON.parse(localStorage.getItem('user')).User_ID;
        this.qualityTablePayload.Created_Date = this.dimentionalService.createdDate;
        // this.qualityTablePayload.Updated_Date = moment().format("YYYY\-MM\-DD");
        this.dimentionalService.customerData.push(this.qualityTablePayload);
        // console.log("type", typeof this.qualityTablePayload)
        setTimeout(function () {
            _this.activeModal.dismiss('Timeout');
        }, 1000);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('instance'),
        __metadata("design:type", _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbTypeahead"])
    ], QualityFormComponent.prototype, "instance", void 0);
    QualityFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-quality-form',
            template: __webpack_require__(/*! ./quality-form.component.html */ "./src/app/modals/quality-form/quality-form.component.html"),
            styles: [__webpack_require__(/*! ./quality-form.component.less */ "./src/app/modals/quality-form/quality-form.component.less")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"], _services_dimentional_service__WEBPACK_IMPORTED_MODULE_4__["DimentionalService"]])
    ], QualityFormComponent);
    return QualityFormComponent;
}());



/***/ }),

/***/ "./src/app/modals/supplier-ppm/supplier-ppm.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/modals/supplier-ppm/supplier-ppm.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\r\n  <div class=\"card-header\">\r\n    <h4>Supplier PPM - Add Entry</h4>\r\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss('Cross click')\" style=\"position: absolute;\r\n    top: 3px;\r\n    right: 10px;\">\r\n      <span aria-hidden=\"true\">&times;</span>\r\n    </button>\r\n  </div>\r\n  <div class=\"card-body\">\r\n    <div class=\"supplier-ppm-form\">\r\n      <div class=\"form-group row\">\r\n        <label for=\"lostTimeIncedent\" class=\"col-sm-9 col-form-label\">Product<font>*</font></label>\r\n        <div class=\"col-sm-3\">\r\n          <input id=\"supplier-productName\" type=\"text\" name=\"product\" class=\"form-control\" [(ngModel)]=\"supplierPPMPayload.Prod_ID\"\r\n            [ngbTypeahead]=\"searchProducts\" (focus)=\"focusProduct$.next($event.target.value)\" (click)=\"clickProduct$.next($event.target.value)\"\r\n            #instance=\"ngbTypeahead\" [ngModelOptions]=\"{standalone: true}\"  #pname=\"ngModel\" required />\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"pname.invalid && (pname.dirty || pname.touched)\">Product Name is required</div>\r\n      </div>\r\n      <div class=\"form-group row\">\r\n        <label for=\"supplierPPM\" class=\"col-sm-9 col-form-label\">Supplier PPM<font>*</font></label>\r\n        <div class=\"col-sm-3\">\r\n          <input type=\"number\" class=\"form-control\" id=\"supplierPPM\" value=\"5\" [(ngModel)]=\"supplierPPMPayload.Tot_Supp_PPM_Qlty\" #sppm=\"ngModel\" required>\r\n        </div>\r\n        <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"sppm.invalid && (sppm.dirty || sppm.touched)\">Supplier PPM is required</div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"card-footer\">\r\n    <button class=\"btn btn-success btn-block mt-2 mb-2\" *ngIf =\" pname.valid && sppm.valid \" (click)=\"addSupplierPPMRecord()\" [ladda]=\"saving\">\r\n      <span class=\"fa fa-fw fa-save\"></span> Save\r\n    </button>\r\n\r\n  </div>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/modals/supplier-ppm/supplier-ppm.component.less":
/*!*****************************************************************!*\
  !*** ./src/app/modals/supplier-ppm/supplier-ppm.component.less ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "font {\n  color: red;\n}\n#sample {\n  color: red;\n}\ninput.submitted.ng-invalid {\n  border: 1px solid black;\n}\n:-moz-ui-invalid:not(output) {\n  box-shadow: none;\n}\n"

/***/ }),

/***/ "./src/app/modals/supplier-ppm/supplier-ppm.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/modals/supplier-ppm/supplier-ppm.component.ts ***!
  \***************************************************************/
/*! exports provided: SupplierPpmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupplierPpmComponent", function() { return SupplierPpmComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_dimentional_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/dimentional.service */ "./src/app/services/dimentional.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SupplierPpmComponent = /** @class */ (function () {
    function SupplierPpmComponent(activeModal, dimentionalService) {
        var _this = this;
        this.activeModal = activeModal;
        this.dimentionalService = dimentionalService;
        this.saving = false;
        this.supplierPPMPayload = {
            Plant_ID: null,
            Benchmark_ID: null,
            User_ID: null,
            Prod_ID: null,
            Emp_Left_Mon: null,
            Tot_Emp_First_Day_Mon: null,
            Tot_Emp_Last_Day_Mon: null,
            Tot_Supp_PPM_Qlty: null,
            Cost_Premium_Freight_In_Mon: null,
            Cost_Premium_Freight_Out_Mon: null,
            Revenue_Mon: null,
            Cogs: null,
            Mon_End_Inv_Local_Curr: null,
            Pre_Mon_End_Inv_Local_Curr: null,
            All_Mfg_Cost_PnL: null,
            Overtime_Cost_PnL: null,
            Created_Date: null,
            Updated_Date: null
            /* product: null,
            supplierPPM: null */
        };
        this.focusProduct$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.clickProduct$ = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.searchProducts = function (text$) {
            var debouncedText$ = text$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["debounceTime"])(200), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["distinctUntilChanged"])());
            var clicksWithClosedPopup$ = _this.clickProduct$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["filter"])(function () { return !_this.instance.isPopupOpen(); }));
            var inputFocus$ = _this.focusProduct$;
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["merge"])(debouncedText$, inputFocus$, clicksWithClosedPopup$).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (term) { return (term === '' ? _this.dimentionalService.products
                : _this.dimentionalService.products.filter(function (v) { return v.toLowerCase().indexOf(term.toLowerCase()) > -1; })).slice(0, 10); }));
        };
    }
    SupplierPpmComponent.prototype.ngOnInit = function () {
    };
    SupplierPpmComponent.prototype.addSupplierPPMRecord = function () {
        var _this = this;
        this.saving = true;
        this.supplierPPMPayload.Plant_ID = localStorage.getItem('plantId');
        this.supplierPPMPayload.Benchmark_ID = this.dimentionalService.benchArray['Quality'];
        this.supplierPPMPayload.Prod_ID = this.dimentionalService.productLookupForID[this.supplierPPMPayload.Prod_ID];
        this.supplierPPMPayload.User_ID = JSON.parse(localStorage.getItem('user')).User_ID;
        this.supplierPPMPayload.Created_Date = this.dimentionalService.createdDate;
        this.dimentionalService.supplierData.push(this.supplierPPMPayload);
        setTimeout(function () {
            _this.activeModal.dismiss('Timeout');
        }, 1000);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('instance'),
        __metadata("design:type", _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbTypeahead"])
    ], SupplierPpmComponent.prototype, "instance", void 0);
    SupplierPpmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-supplier-ppm',
            template: __webpack_require__(/*! ./supplier-ppm.component.html */ "./src/app/modals/supplier-ppm/supplier-ppm.component.html"),
            styles: [__webpack_require__(/*! ./supplier-ppm.component.less */ "./src/app/modals/supplier-ppm/supplier-ppm.component.less")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NgbActiveModal"], _services_dimentional_service__WEBPACK_IMPORTED_MODULE_4__["DimentionalService"]])
    ], SupplierPpmComponent);
    return SupplierPpmComponent;
}());



/***/ }),

/***/ "./src/app/new-report/new-report.component.html":
/*!******************************************************!*\
  !*** ./src/app/new-report/new-report.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container new-report-wrapper\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md\">\r\n      <div class=\"section-heading mb-3\">\r\n        <div class=\"header\">\r\n          <span class=\"h2\">Create New Report</span>\r\n          <!-- <span class=\"float-right h4\"><span class=\"text-muted\">Region:</span> 3</span>\r\n          <span class=\"float-right h4 mr-3\"><span class=\"text-muted\">Plant:</span> 6</span> [(ngModel)]=\"reportObj.plant[0].Lost_Time_Incident\" -->\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"row\">\r\n        <div class=\"col-md-7\">\r\n          <div class=\"card mb-4 safety-card\">\r\n            <div class=\"card-body\">\r\n              <h3 class=\"card-title\">\r\n                <span class=\"card-title-text card-title-text-safety\">Safety Parameters</span>\r\n              </h3>\r\n              <div class=\"card-contents mt-4\">\r\n                <form novalidate>\r\n                  <div class=\"form-group row\">\r\n                    <label class=\"col-sm-9 col-form-label\"># of Lost Time Incidents for the\r\n                      Month<font>*</font></label>\r\n                    <div class=\"col-sm-3 text-center\">\r\n                      <input type=\"number\" class=\"form-control\" name=\"lostTimeIncedent\"\r\n                             [(ngModel)]=\"dimentionalService.safetyPrameters.Lost_Time_Incident\"\r\n                             [ngClass]=\"{'form-control-plaintext' : isInReview, 'form-control' : !isInReview}\"\r\n                             [readonly]=\"isInReview\" #ltime=\"ngModel\" required>\r\n                    </div>\r\n                    <!-- pattern=\"^[1-9][0-9]{0,9}$\" #lostTimeIncedent=\"ngModel\" <div *ngIf=\"lostTimeIncedent.touched\">\r\n                      <div *ngIf=\"lostTimeIncedent.errors?.pattern\" class=\"text-danger\">\r\n                        *this entry can only contain number!!\r\n                      </div>\r\n                    </div> -->\r\n                    <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"ltime.invalid && (ltime.dirty || ltime.touched)\"># of Lost\r\n                      Time Incidents for the\r\n                      Month is required\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group row\">\r\n                    <label class=\"col-sm-9 col-form-label\">Total # of lost time days for the\r\n                      Month<font>*</font></label>\r\n                    <div class=\"col-sm-3\">\r\n                      <input type=\"number\" name=\"lostDaysPrevMonth\"\r\n                             [(ngModel)]=\"dimentionalService.safetyPrameters.Tot_Lost_Time_Days_Mon\"\r\n                             [ngClass]=\"{'form-control-plaintext' : isInReview, 'form-control' : !isInReview}\"\r\n                             [readonly]=\"isInReview\" #totltime=\"ngModel\" required>\r\n                    </div>\r\n                    <div id=\"sample\" class=\"col-sm-9\" *ngIf=\"totltime.invalid && (totltime.dirty || totltime.touched)\">\r\n                      Total # of lost time days for the\r\n                      Month is required\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"form-group row\">\r\n                    <label class=\"col-sm-9 col-form-label\">Total Employees on the first day of\r\n                      the Month<font>*</font></label>\r\n                    <div class=\"col-sm-3\">\r\n                      <input type=\"number\" name=\"empFirstDayofMonth\"\r\n                             [(ngModel)]=\"dimentionalService.safetyPrameters.Tot_Emp_First_Day_Mon\"\r\n                             [ngClass]=\"{'form-control-plaintext' : isInReview, 'form-control' : !isInReview}\"\r\n                             [readonly]=\"isInReview\" #totempfirst=\"ngModel\" required>\r\n                    </div>\r\n                    <div id=\"sample\" class=\"col-sm-9\"\r\n                         *ngIf=\"totempfirst.invalid && (totempfirst.dirty || totempfirst.touched)\">Total Employees on\r\n                      the first day of\r\n                      the Month is required\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"form-group row\">\r\n                    <label class=\"col-sm-9 col-form-label\">Total Employees on last day of the\r\n                      Month<font>*</font></label>\r\n                    <div class=\"col-sm-3\">\r\n                      <input type=\"number\" name=\"empLastDayofMonth\"\r\n                             [(ngModel)]=\"dimentionalService.safetyPrameters.Tot_Emp_Last_Day_Mon\"\r\n                             [ngClass]=\"{'form-control-plaintext' : isInReview, 'form-control' : !isInReview}\"\r\n                             [readonly]=\"isInReview\" #totemplast=\"ngModel\" required>\r\n                    </div>\r\n                    <div id=\"sample\" class=\"col-sm-9\"\r\n                         *ngIf=\"totemplast.invalid && (totemplast.dirty || totemplast.touched)\">Total Employees on last\r\n                      day of the\r\n                      Month is required\r\n                    </div>\r\n                  </div>\r\n\r\n                </form>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-5 right-info-panel text-center\" style=\"padding-top: 30px\"\r\n             [ngClass]=\"{'non-review-padding' : !isInReview}\">\r\n          <div class=\"review-flag pb-3\" *ngIf=\"isInReview\">\r\n            <span class=\"text-danger h5\">In-Review</span>\r\n          </div>\r\n          <div class=\"info-month mb-3\">\r\n            <!-- <p>{{today | date : 'MMM dd, yyyy'}} </p> -->\r\n            <span class=\"text-muted\">\r\n              <span *ngIf=\"!isInReview\">Filing</span>\r\n              <span *ngIf=\"isInReview\">Reviewing</span> report For:</span>\r\n            <div>\r\n              <span class=\"month capitalize\">{{selectedMonth}}</span>\r\n              <br>\r\n              <p class=\"h5\">{{selectedYear}}</p>\r\n            </div>\r\n          </div>\r\n          <hr>\r\n          <div class=\"row\">\r\n            <div class=\"col-sm-4 info-region\">\r\n              Region\r\n              <br/>\r\n              <strong>{{dimentionalService.regionName}}</strong>\r\n            </div>\r\n            <div class=\"col-sm-8 info-plant\">\r\n              Plant\r\n              <br/>\r\n              <strong>{{dimentionalService.plantName}}</strong>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"card mb-4 quality-card\">\r\n        <div class=\"card-body\">\r\n          <h3 class=\"card-title\">\r\n            <span class=\"card-title-text card-title-text-quality\">Quality</span>\r\n          </h3>\r\n          <div class=\"card-contents mt-4\">\r\n            <div class=\"card-contents-header mb-2\">\r\n              <div class=\"float-right\">\r\n                <h4>Customer KPI</h4>\r\n              </div>\r\n            </div>\r\n            <table class=\"table mb-5\">\r\n              <thead class=\"small\">\r\n              <tr valign=\"top\">\r\n                <th scope=\"col\">#</th>\r\n                <th scope=\"col\">Customer</th>\r\n                <th scope=\"col\">Product</th>\r\n                <th scope=\"col\">Sub-Product</th>\r\n                <th scope=\"col\">Total Rejected Qty for the month</th>\r\n                <th scope=\"col\">Shipped Quanitity for the month</th>\r\n                <th scope=\"col\">$ Production Scrap for the month</th>\r\n                <th scope=\"col\">$ Revenue for the month</th>\r\n                <th scope=\"col\"># of Complaints</th>\r\n                <th scope=\"col\" *ngIf=\"!isInReview\">Actions</th>\r\n              </tr>\r\n              </thead>\r\n              <tbody>\r\n              <tr *ngIf=\"dimentionalService.customerData.length == 0\">\r\n                <td colspan=\"11\" class=\"text-center p-5\">\r\n                  <span class=\"h5\" style=\"color: #ccc\">No records added</span>\r\n                </td>\r\n              </tr>\r\n              <tr *ngFor=\"let qualityReport of dimentionalService.customerData; let index = index\">\r\n                <th scope=\"row\">{{index + 1}}</th>\r\n                <td>{{dimentionalService.customerLookup[qualityReport.Cust_ID]}}</td>\r\n                <td>{{dimentionalService.productLookup[qualityReport.Prod_ID]}}</td>\r\n                <td>{{dimentionalService.subProductLookup[qualityReport.Sub_Prod_ID]}}</td>\r\n                <td>{{qualityReport.Tot_Rjctd_Qty_For_Mon}}</td>\r\n                <td>{{qualityReport.Shipped_Qty_For_Mon}}</td>\r\n                <td>{{qualityReport.Scrap_For_Mon}}</td>\r\n                <td>{{qualityReport.Revenue_Mon}}</td>\r\n                <td>{{qualityReport.No_Of_Complaints}}</td>\r\n                <td class=\"row-actions\" *ngIf=\"!isInReview\" style=\"min-width: 77px\">\r\n                  <!--<span class=\"fa fw-fw fa-pencil\"></span>-->\r\n                  <span class=\"fa fw-fw fa-trash\" (click)=\"delete(dimentionalService.customerData,index)\"></span>\r\n                </td>\r\n              </tr>\r\n\r\n              <tr *ngIf=\"!isInReview\">\r\n                <td colspan=\"11\">\r\n                  <button class=\"btn btn-block btn-outline-primary\" (click)=\"openModal('QualityFormComponent')\">\r\n                    <span class=\"fa fa-fw fa-plus\"></span>Add New\r\n                  </button>\r\n                </td>\r\n              </tr>\r\n              </tbody>\r\n            </table>\r\n\r\n            <div class=\"card-contents mt-4\">\r\n              <div class=\"card-contents-header mb-2\">\r\n                <div class=\"float-right\">\r\n                  <h4>Supplier PPM</h4>\r\n                </div>\r\n              </div>\r\n              <div class=\"table-responsive\">\r\n                <table class=\"table\">\r\n                  <thead class=\"small\">\r\n                  <tr>\r\n                    <th scope=\"col\">#</th>\r\n                    <th scope=\"col\">Product</th>\r\n                    <th scope=\"col\">Total Supplier # PPM</th>\r\n                    <th scope=\"col\" *ngIf=\"!isInReview\">Actions</th>\r\n                  </tr>\r\n                  </thead>\r\n                  <tbody>\r\n                  <tr *ngIf=\"dimentionalService.supplierData.length == 0\">\r\n                    <td colspan=\"13\" class=\"text-center p-5\">\r\n                      <span class=\"h5\" style=\"color: #ccc\">No records added</span>\r\n                    </td>\r\n                  </tr>\r\n                  <tr *ngFor=\"let supplierReport of dimentionalService.supplierData; let index = index\">\r\n                    <th scope=\"row\">{{index + 1}}</th>\r\n                    <td>{{dimentionalService.productLookup[supplierReport.Prod_ID]}}</td>\r\n                    <td>{{supplierReport.Tot_Supp_PPM_Qlty}}</td>\r\n                    <td class=\"row-actions\" *ngIf=\"!isInReview\" style=\"min-width: 77px\">\r\n                      <!--<span class=\"fa fw-fw fa-pencil\"></span>-->\r\n                      <span class=\"fa fw-fw fa-trash\" (click)=\"delete(dimentionalService.supplierData,index)\"></span>\r\n                    </td>\r\n                  </tr>\r\n                  <tr *ngIf=\"!isInReview\">\r\n                    <td colspan=\"13\">\r\n                      <button class=\"btn btn-block btn-outline-primary\" (click)=\"openModal('SupplierPpmComponent')\">\r\n                        <span class=\"fa fa-fw fa-plus\"></span>Add New\r\n                      </button>\r\n                    </td>\r\n                  </tr>\r\n                  </tbody>\r\n                </table>\r\n              </div>\r\n            </div>\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"card mb-4 productivity-card\">\r\n        <div class=\"card-body\">\r\n          <h3 class=\"card-title\">\r\n            <span class=\"card-title-text card-title-text-productivity\">Productivity</span>\r\n          </h3>\r\n\r\n          <div class=\"card-contents mt-4\">\r\n            <div class=\"card-contents-header mb-2\">\r\n              <div class=\"float-right\">\r\n                <h4>On-time Delivery</h4>\r\n              </div>\r\n            </div>\r\n            <table class=\"table\">\r\n              <thead class=\"small\">\r\n              <tr>\r\n                <th scope=\"col\">#</th>\r\n                <th scope=\"col\">Product</th>\r\n                <th scope=\"col\">Customer</th>\r\n                <th scope=\"col\">Qty of conforming products shipped for the month</th>\r\n                <th scope=\"col\">Total Qty of Product Shipped for the month</th>\r\n                <th scope=\"col\" *ngIf=\"!isInReview\">Actions</th>\r\n              </tr>\r\n              </thead>\r\n              <tbody>\r\n              <tr *ngIf=\"dimentionalService.onTimeDeliveryData.length == 0\">\r\n                <td colspan=\"10\" class=\"text-center p-5\">\r\n                  <span class=\"h5\" style=\"color: #ccc\">No records added</span>\r\n                </td>\r\n              </tr>\r\n              <tr *ngFor=\"let onTimeDelivery of dimentionalService.onTimeDeliveryData; let index = index\">\r\n                <th scope=\"row\">{{index + 1}}</th>\r\n                <td>{{dimentionalService.productLookup[onTimeDelivery.Prod_ID]}}</td>\r\n                <td>{{dimentionalService.customerLookup[onTimeDelivery.Cust_ID]}}</td>\r\n                <td>{{onTimeDelivery.Cnf_Prod_Shipped_Mon}}</td>\r\n                <td>{{onTimeDelivery.Tot_Prod_Shipped_Mon}}</td>\r\n                <td class=\"row-actions\" *ngIf=\"!isInReview\" style=\"min-width: 77px\">\r\n                  <!--<span class=\"fa fw-fw fa-pencil\"></span>-->\r\n                  <span class=\"fa fw-fw fa-trash\" (click)=\"delete(dimentionalService.onTimeDeliveryData,index)\"></span>\r\n                </td>\r\n              </tr>\r\n              <tr *ngIf=\"!isInReview\">\r\n                <td colspan=\"6\">\r\n                  <button class=\"btn btn-block btn-outline-primary\" (click)=\"openModal('OnTimeDeliveryFormComponent')\">\r\n                    <span class=\"fa fa-fw fa-plus\"></span>Add New\r\n                  </button>\r\n                </td>\r\n              </tr>\r\n              </tbody>\r\n            </table>\r\n          </div>\r\n\r\n          <div class=\"card-contents mt-5\">\r\n            <div class=\"card-contents-header mb-2\">\r\n              <div class=\"float-right\">\r\n                <h4>Productivity KPI</h4>\r\n              </div>\r\n            </div>\r\n            <div class=\"table-responsive\">\r\n              <table class=\"table\">\r\n                <thead class=\"small\">\r\n                <tr>\r\n                  <th scope=\"col\">#</th>\r\n                  <th scope=\"col\">Product</th>\r\n                  <th scope=\"col\"># of employees who left during the month</th>\r\n                  <th scope=\"col\">Total Employees\r\n                    <br> on first day of the month\r\n                  </th>\r\n                  <th scope=\"col\">Total Employees on last day the month</th>\r\n                  <th scope=\"col\">$ Cost of premium freight In</th>\r\n                  <th scope=\"col\">$ Cost of premium freight Out</th>\r\n                  <th scope=\"col\">$ Revenue for the month</th>\r\n                  <th scope=\"col\">COGS</th>\r\n                  <th scope=\"col\">Month end inventory value in local currency</th>\r\n                  <th scope=\"col\">Previous Month-End inventory value in local currency</th>\r\n                  <th scope=\"col\">MFG Costs from P&L</th>\r\n                  <th scope=\"col\">Overtime Costs from P&L</th>\r\n\r\n                  <th scope=\"col\" *ngIf=\"!isInReview\">Actions</th>\r\n                </tr>\r\n                </thead>\r\n                <tbody>\r\n                <tr *ngIf=\"dimentionalService.productivityData.length == 0\">\r\n                  <td colspan=\"13\" class=\"text-center p-5\">\r\n                    <span class=\"h5\" style=\"color: #ccc\">No records added</span>\r\n                  </td>\r\n                </tr>\r\n                <tr *ngFor=\"let productivityReport of dimentionalService.productivityData; let index = index\">\r\n                  <th scope=\"row\">{{index + 1}}</th>\r\n                  <td>{{dimentionalService.productLookup[productivityReport.Prod_ID]}}</td>\r\n                  <td>{{productivityReport.Emp_Left_Mon}}</td>\r\n                  <td>{{productivityReport.Tot_Emp_First_Day_Mon}}</td>\r\n                  <td>{{productivityReport.Tot_Emp_Last_Day_Mon}}</td>\r\n                  <td>{{productivityReport.Cost_Premium_Freight_In_Mon}}</td>\r\n                  <td>{{productivityReport.Cost_Premium_Freight_Out_Mon}}</td>\r\n                  <td>{{productivityReport.Revenue_Mon}}</td>\r\n                  <td>{{productivityReport.Cogs}}</td>\r\n                  <td>{{productivityReport.Mon_End_Inv_Local_Curr}}</td>\r\n                  <td>{{productivityReport.Pre_Mon_End_Inv_Local_Curr}}</td>\r\n                  <td>{{productivityReport.All_Mfg_Cost_PnL}}</td>\r\n                  <td>{{productivityReport.Overtime_Cost_PnL}}</td>\r\n                  <td class=\"row-actions\" *ngIf=\"!isInReview\" style=\"min-width: 77px\">\r\n                    <!--<span class=\"fa fw-fw fa-pencil\"></span>-->\r\n                    <span class=\"fa fw-fw fa-trash\" (click)=\"delete(dimentionalService.productivityData,index)\"></span>\r\n                  </td>\r\n                </tr>\r\n                <tr *ngIf=\"!isInReview\">\r\n                  <td colspan=\"15\">\r\n                    <button class=\"btn btn-block btn-outline-primary\"\r\n                            (click)=\"openModal('ProductivityKpiFormComponent')\">\r\n                      <span class=\"fa fa-fw fa-plus\"></span>Add New\r\n                    </button>\r\n                  </td>\r\n                </tr>\r\n                </tbody>\r\n              </table>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n      </div>\r\n\r\n\r\n      <div *ngIf=\"!viewOnly\">\r\n        <span class=\"back-button mb-5\">\r\n          <button class=\"btn btn-lg btn-warning\" *ngIf=\"isInReview\" (click)=\"isInReview = false\">\r\n            <span class=\"fa fa-fw fa-arrow-left\"></span>\r\n            Back : Edit\r\n          </button>\r\n        </span>\r\n\r\n        <span class=\"save-button mb-5 float-right\">\r\n          <!-- shows only on editable screen -->\r\n          <button class=\"btn btn-lg btn-success\" *ngIf=\"!isInReview\" (click)=\"pushForReview()\">\r\n            Next Step : Review\r\n            <span class=\"fa fa-fw fa-arrow-right\"></span>\r\n          </button>\r\n\r\n          <!-- shows only on review screen -->\r\n          <button class=\"btn btn-lg btn-success\" *ngIf=\"isInReview\" (click)=\"save()\"\r\n                  [disabled]=\"!checkFormValidation()\">\r\n            <span class=\"fa fa-fw fa-save\"></span>\r\n            Confirm and Save\r\n          </button>\r\n        </span>\r\n      </div>\r\n\r\n      <div *ngIf=\"viewOnly\" class=\"mb-4\">\r\n        <span class=\"delete-button float-right\">\r\n          <button class=\"btn btn-block btn-danger btn-lg\" (click)=\"openModal('DeleteConfirmation')\">\r\n            <span class=\"fa fa-fw fa-trash\"></span> Delete this report\r\n          </button>\r\n        </span>\r\n\r\n        <span class=\"delete-button\">\r\n          <button class=\"btn btn-secondary btn-lg\" (click)=\"router.navigate(['']);\">\r\n            <span class=\"fa fa-fw fa-arrow-left\"></span> Go Back\r\n          </button>\r\n        </span>\r\n\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/new-report/new-report.component.less":
/*!******************************************************!*\
  !*** ./src/app/new-report/new-report.component.less ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".capitalize {\n  text-transform: capitalize;\n}\n.new-report-wrapper {\n  margin-top: 50px;\n}\n.new-report-wrapper .card-title-text {\n  padding-bottom: 3px;\n  border-bottom: 3px solid;\n}\n.new-report-wrapper .card-title-text-safety {\n  border-color: red;\n}\n.new-report-wrapper .card-title-text-quality {\n  border-color: orange;\n}\n.new-report-wrapper .card-title-text-productivity {\n  border-color: purple;\n}\n.new-report-wrapper .card-contents {\n  padding: 1rem;\n}\n.new-report-wrapper .right-info-panel {\n  border-left: 2px dashed #ccc;\n  padding-left: 2rem;\n}\n.new-report-wrapper .right-info-panel .info-month .month {\n  font-size: 60px;\n}\n.new-report-wrapper .right-info-panel .info-region,\n.new-report-wrapper .right-info-panel .info-plant {\n  font-size: 25px;\n}\n.new-report-wrapper table .row-actions span {\n  padding: 0px 5px;\n}\n.new-report-wrapper .non-review-padding {\n  padding-top: 70px !important;\n}\nfont {\n  color: red;\n}\n#sample {\n  color: red;\n}\ninput.submitted.ng-invalid {\n  border: 1px solid black;\n}\n:-moz-ui-invalid:not(output) {\n  box-shadow: none;\n}\n"

/***/ }),

/***/ "./src/app/new-report/new-report.component.ts":
/*!****************************************************!*\
  !*** ./src/app/new-report/new-report.component.ts ***!
  \****************************************************/
/*! exports provided: NewReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewReportComponent", function() { return NewReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _modals_on_time_delivery_form_on_time_delivery_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../modals/on-time-delivery-form/on-time-delivery-form.component */ "./src/app/modals/on-time-delivery-form/on-time-delivery-form.component.ts");
/* harmony import */ var _modals_productivity_kpi_form_productivity_kpi_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../modals/productivity-kpi-form/productivity-kpi-form.component */ "./src/app/modals/productivity-kpi-form/productivity-kpi-form.component.ts");
/* harmony import */ var _modals_quality_form_quality_form_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../modals/quality-form/quality-form.component */ "./src/app/modals/quality-form/quality-form.component.ts");
/* harmony import */ var _modals_supplier_ppm_supplier_ppm_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../modals/supplier-ppm/supplier-ppm.component */ "./src/app/modals/supplier-ppm/supplier-ppm.component.ts");
/* harmony import */ var _modals_delete_confirmation_delete_confirmation_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../modals/delete-confirmation/delete-confirmation.component */ "./src/app/modals/delete-confirmation/delete-confirmation.component.ts");
/* harmony import */ var _services_dimentional_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/dimentional.service */ "./src/app/services/dimentional.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_10__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var NewReportComponent = /** @class */ (function () {
    function NewReportComponent(modalService, activatedRoute, router, dimentionalService, location) {
        this.modalService = modalService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.dimentionalService = dimentionalService;
        this.location = location;
        this.selectedYear = null;
        this.selectedMonth = null;
        this.today = new Date();
        this.isInReview = false;
        this.viewOnly = false;
        this.formStatuses = {
            plantSet: true,
            safety: true,
            onTimeDeliv: true,
            productivityKPI: true
        };
        if (this.activatedRoute.snapshot.queryParams.viewOnly === 'true') {
            this.viewOnly = true;
        }
        this.selectedYear = this.activatedRoute.snapshot.params.year;
        this.selectedMonthInNum = this.activatedRoute.snapshot.params.month;
        this.plantId = localStorage.getItem('plantId');
        this.selectedMonth = moment__WEBPACK_IMPORTED_MODULE_10__().month(parseInt(this.selectedMonthInNum) - 1).format('MMMM');
        // this.createdDate = moment().year(this.selectedYear).month(parseInt(this.selectedMonthInNum)-1).day(1).format("YYYY\-MM\-DD")
        // this.dimentionalService.createdDate = this.selectedYear+"-"+(this.selectedMonthInNum<10?"0"+this.selectedMonthInNum:this.selectedMonthInNum)+"-01";
        // console.log(this.activatedRoute.snapshot.params.month, this.selectedMonthInNum, this.selectedMonth, this.dimentionalService.createdDate);
        this.displayReports(this.selectedYear, this.selectedMonthInNum, this.plantId);
    }
    NewReportComponent.prototype.ngOnInit = function () {
        if (this.viewOnly) {
            this.isInReview = true;
        }
    };
    NewReportComponent.prototype.displayReports = function (year, month, plantId) {
        var _this = this;
        var date = year + '-' + (month < 10 ? '0' + month : month) + '-01';
        this.dimentionalService.createdDate = date;
        this.dimentionalService.getReportsForMonth(date, plantId).subscribe(function (reports) {
            _this.reportObj = reports;
            _this.dimentionalService.customerData = [];
            _this.dimentionalService.supplierData = [];
            _this.dimentionalService.onTimeDeliveryData = [];
            _this.dimentionalService.productivityData = [];
            for (var key in _this.reportObj) {
                if (key == 'plant' && _this.reportObj.plant.length) {
                    _this.dimentionalService.safetyPrameters = _this.reportObj.plant[0];
                }
                else if (key == 'customer') {
                    var i;
                    for (i = 0; i < _this.reportObj.customer.length; i++) {
                        if (_this.reportObj.customer[i].Benchmark_ID == 'BM003') {
                            _this.dimentionalService.onTimeDeliveryData.push(_this.reportObj.customer[i]);
                        }
                        else if (_this.reportObj.customer[i].Benchmark_ID == 'BM002') {
                            _this.dimentionalService.customerData.push(_this.reportObj.customer[i]);
                        }
                    }
                }
                else if (key == 'product') {
                    var i;
                    for (i = 0; i < _this.reportObj.product.length; i++) {
                        if (_this.reportObj.product[i].Benchmark_ID == 'BM003') {
                            _this.dimentionalService.productivityData.push(_this.reportObj.product[i]);
                        }
                        else if (_this.reportObj.product[i].Benchmark_ID == 'BM002') {
                            _this.dimentionalService.supplierData.push(_this.reportObj.product[i]);
                        }
                    }
                }
            }
        });
    };
    NewReportComponent.prototype.openModal = function (modalComponent) {
        switch (modalComponent) {
            case 'OnTimeDeliveryFormComponent':
                var modalRef = this.modalService.open(_modals_on_time_delivery_form_on_time_delivery_form_component__WEBPACK_IMPORTED_MODULE_4__["OnTimeDeliveryFormComponent"]);
                break;
            case 'ProductivityKpiFormComponent':
                var modalRef = this.modalService.open(_modals_productivity_kpi_form_productivity_kpi_form_component__WEBPACK_IMPORTED_MODULE_5__["ProductivityKpiFormComponent"]);
                break;
            case 'QualityFormComponent':
                var modalRef = this.modalService.open(_modals_quality_form_quality_form_component__WEBPACK_IMPORTED_MODULE_6__["QualityFormComponent"]);
                break;
            case 'SupplierPpmComponent':
                var modalRef = this.modalService.open(_modals_supplier_ppm_supplier_ppm_component__WEBPACK_IMPORTED_MODULE_7__["SupplierPpmComponent"]);
                break;
            case 'DeleteConfirmation':
                var modalRef = this.modalService.open(_modals_delete_confirmation_delete_confirmation_component__WEBPACK_IMPORTED_MODULE_8__["DeleteConfirmationComponent"]);
                modalRef.componentInstance.date = this.dimentionalService.safetyPrameters.Created_Date;
                modalRef.componentInstance.plantId = this.dimentionalService.plantId;
                break;
        }
        ;
    };
    /* checkIfPlantSet() {
      if (this.selectedPlant == "Select Plant") {
        this.formStatuses.plantSet = false;
        return "No Plant Selected";
      }
      else {
        this.formStatuses.plantSet = true;
        return this.selectedPlant
      }
    } */
    /* checkIfPlantSetBoolean() {
      if (this.selectedPlant == "Select Plant") {
        return true
      }
      else {
        return false
      }
    } */
    NewReportComponent.prototype.delete = function (array, index) {
        array.splice(index, 1);
    };
    NewReportComponent.prototype.pushForReview = function () {
        this.isInReview = true;
        window.scrollTo(0, 0);
        this.dimentionalService.safetyPrameters.Plant_ID = localStorage.getItem('plantId');
        this.dimentionalService.safetyPrameters.Benchmark_ID = this.dimentionalService.benchArray['Safety'];
        this.dimentionalService.safetyPrameters.User_ID = JSON.parse(localStorage.getItem('user')).User_ID;
        this.dimentionalService.safetyPrameters.Created_Date = this.dimentionalService.createdDate;
        /* console.log("Safety", this.dimentionalService.safetyPrameters)
        console.log("customer", this.dimentionalService.customerData)
        console.log("supplier", this.dimentionalService.supplierData)
        console.log("OnTimeDelivery", this.dimentionalService.onTimeDeliveryData)
        console.log("productivityData", this.dimentionalService.productivityData) */
    };
    NewReportComponent.prototype.checkFormValidation = function () {
        if (this.formStatuses.plantSet && this.formStatuses.safety && this.formStatuses.onTimeDeliv && this.formStatuses.productivityKPI) {
            return true;
        }
    };
    NewReportComponent.prototype.save = function () {
        this.customerPayloadArray = this.dimentionalService.customerData.concat(this.dimentionalService.onTimeDeliveryData);
        this.productPayloadArray = this.dimentionalService.supplierData.concat(this.dimentionalService.productivityData);
        /* Array.prototype.push.apply(this.dimentionalService.customerData, this.dimentionalService.onTimeDeliveryData);*/
        this.dimentionalService.addPlantReport(this.dimentionalService.safetyPrameters).subscribe(function (result) {
            console.log('result', result);
        });
        this.dimentionalService.addCustomerReport(this.customerPayloadArray).subscribe(function (result) {
            console.log('result', result);
        });
        this.dimentionalService.addProductReport(this.productPayloadArray).subscribe(function (result) {
            console.log('result', result);
        });
        console.log('Plant KPI', this.dimentionalService.safetyPrameters);
        console.log('customer KPI', this.customerPayloadArray);
        console.log('supplier KPI', this.productPayloadArray);
        this.router.navigate(['../']);
    };
    NewReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-new-report',
            template: __webpack_require__(/*! ./new-report.component.html */ "./src/app/new-report/new-report.component.html"),
            styles: [__webpack_require__(/*! ./new-report.component.less */ "./src/app/new-report/new-report.component.less")]
        }),
        __metadata("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModal"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _services_dimentional_service__WEBPACK_IMPORTED_MODULE_9__["DimentionalService"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["Location"]])
    ], NewReportComponent);
    return NewReportComponent;
}());



/***/ }),

/***/ "./src/app/services/auth.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/auth.service.ts ***!
  \******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
        this.baseUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl;
    }
    AuthService.prototype.checkLoginStatus = function () {
        var user = JSON.parse(localStorage.getItem('user'));
        if (user && user.token) {
            return true;
        }
        else {
            return false;
        }
        ;
    };
    AuthService.prototype.getLoggedInUser = function () {
        return JSON.parse(localStorage.getItem('user'));
    };
    AuthService.prototype.login = function (username, password) {
        var payload = {
            'username': username,
            'password': password
        };
        return this.http.post(this.baseUrl + "login", payload);
    };
    AuthService.prototype.logout = function () {
        localStorage.removeItem('user');
        localStorage.removeItem('plantId');
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/services/dimentional.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/dimentional.service.ts ***!
  \*************************************************/
/*! exports provided: DimentionalService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DimentionalService", function() { return DimentionalService; });
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var DimentionalService = /** @class */ (function () {
    function DimentionalService(http) {
        var _this = this;
        this.http = http;
        this.baseUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].baseUrl;
        this.benchArray = {};
        this.productLookup = {};
        this.subProductLookup = {};
        this.customerLookup = {};
        this.productLookupForID = {};
        this.subProductLookupForID = {};
        this.customerLookupForID = {};
        this.userId = JSON.parse(localStorage.getItem('user')).User_ID;
        this.safetyPrameters = {
            Plant_ID: null,
            Benchmark_ID: null,
            User_ID: null,
            Tot_Emp_First_Day_Mon: null,
            Tot_Emp_Last_Day_Mon: null,
            Tot_Lost_Time_Days_Mon: null,
            Lost_Time_Incident: null,
            Created_Date: null,
            Updated_Date: null
        };
        this.customerData = [];
        this.supplierData = [];
        this.onTimeDeliveryData = [];
        this.productivityData = [];
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        this.getProducts().subscribe(function (result) {
            // get the results and filter subproducts and products
            // trim these of the Char whitespace
            var arrayedProducts = lodash__WEBPACK_IMPORTED_MODULE_0__["map"](result, function (x) {
                _this.productLookup[_this.trimmer(x, 'Prod_ID')] = _this.trimmer(x, 'Prod_Type');
                _this.productLookupForID[_this.trimmer(x, 'Prod_Type')] = _this.trimmer(x, 'Prod_ID');
                return _this.trimmer(x, 'Prod_Type');
            });
            _this.products = lodash__WEBPACK_IMPORTED_MODULE_0__["uniq"](arrayedProducts);
            _this.getSubProducts().subscribe(function (result) {
                var arrayedSubProducts = lodash__WEBPACK_IMPORTED_MODULE_0__["map"](result, function (x) {
                    _this.subProductLookup[_this.trimmer(x, 'Sub_Prod_ID')] = _this.trimmer(x, 'Sub_Prod_Type');
                    _this.subProductLookupForID[_this.trimmer(x, 'Sub_Prod_Type')] = _this.trimmer(x, 'Sub_Prod_ID');
                    return _this.trimmer(x, 'Sub_Prod_Type');
                });
                _this.subProducts = arrayedSubProducts;
                _this.getCustomers().subscribe(function (result) {
                    _this.customers = lodash__WEBPACK_IMPORTED_MODULE_0__["map"](result, function (x) {
                        _this.customerLookup[_this.trimmer(x, 'Cust_ID')] = _this.trimmer(x, 'Cust_Name');
                        _this.customerLookupForID[_this.trimmer(x, 'Cust_Name')] = _this.trimmer(x, 'Cust_ID');
                        return _this.trimmer(x, 'Cust_Name');
                        /* return {"name":this.trimmer(x,'Cust_Name'),
                                "id":this.trimmer(x, 'Cust_ID')
                        } */
                    });
                    _this.getBenchmarks().subscribe(function (result) {
                        _this.benchmarks = lodash__WEBPACK_IMPORTED_MODULE_0__["map"](result, function (x) {
                            _this.benchArray[_this.trimmer(x, 'Benchmark_Name')] = _this.trimmer(x, 'Benchmark_ID');
                        });
                    });
                });
            });
        });
    }
    DimentionalService.prototype.getIDFromString = function (str) {
        return str.split('- ')[1];
    };
    DimentionalService.prototype.getProducts = function () {
        return this.http.get(this.baseUrl + "products");
        // , post, {headers: this.headers}
    };
    DimentionalService.prototype.getSubProducts = function () {
        return this.http.get(this.baseUrl + "subproducts");
        // , post, {headers: this.headers}
    };
    DimentionalService.prototype.getCustomers = function () {
        return this.http.get(this.baseUrl + "customers");
    };
    DimentionalService.prototype.getBenchmarks = function () {
        return this.http.get(this.baseUrl + "benchmarks");
    };
    DimentionalService.prototype.getPlants = function () {
        return this.http.get(this.baseUrl + "plants");
    };
    /*  getRecordsHistory() {
       return this.http.get(this.baseUrl + `plants`);
     } */
    DimentionalService.prototype.getReportsPresentForYear = function (year, plantId) {
        return this.http.get(this.baseUrl + "checkreports?year=" + year + "&plantId=" + plantId);
    };
    DimentionalService.prototype.getReportsForMonth = function (date, plantId) {
        return this.http.get(this.baseUrl + "report?date=" + date + "&plantId=" + plantId);
    };
    DimentionalService.prototype.addPlantReport = function (rowData) {
        return this.http.post(this.baseUrl + "plants", rowData, { headers: this.headers, responseType: 'text' });
    };
    DimentionalService.prototype.addCustomerReport = function (rowData) {
        return this.http.post(this.baseUrl + "customers", rowData, { headers: this.headers, responseType: 'text' });
    };
    DimentionalService.prototype.addProductReport = function (rowData) {
        return this.http.post(this.baseUrl + "products", rowData, { headers: this.headers, responseType: 'text' });
    };
    DimentionalService.prototype.deleteRecord = function (date, plantId) {
        return this.http.delete(this.baseUrl + "delete?date=" + date + "&plantId=" + plantId, { headers: this.headers, responseType: 'text' });
    };
    DimentionalService.prototype.trimmer = function (elem, key) {
        return elem[key].trim();
    };
    DimentionalService.prototype.resetFields = function () {
        this.safetyPrameters = {
            Plant_ID: null,
            Benchmark_ID: null,
            User_ID: null,
            Tot_Emp_First_Day_Mon: null,
            Tot_Emp_Last_Day_Mon: null,
            Tot_Lost_Time_Days_Mon: null,
            Lost_Time_Incident: null,
            Created_Date: null,
            Updated_Date: null
        };
        this.customerData = [];
        this.supplierData = [];
        this.onTimeDeliveryData = [];
        this.productivityData = [];
    };
    DimentionalService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], DimentionalService);
    return DimentionalService;
}());



/***/ }),

/***/ "./src/app/topbar/topbar.component.html":
/*!**********************************************!*\
  !*** ./src/app/topbar/topbar.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-light mb-5\" style=\"border-bottom: 2px solid navy\">\r\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarTogglerDemo01\" aria-controls=\"navbarTogglerDemo01\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n    <span class=\"navbar-toggler-icon\"></span>\r\n  </button>\r\n  <div class=\"collapse navbar-collapse\" id=\"navbarTogglerDemo01\">\r\n    <a class=\"navbar-brand\" href=\"#\">\r\n      <img src=\"../assets/img/whiteLogoC.png\" alt=\"\" height=\"60px\">\r\n    </a>\r\n    <ul class=\"navbar-nav mr-auto mt-2 mt-lg-0\">\r\n      <li class=\"nav-item active\">\r\n        <a class=\"nav-link\" [routerLink]=\"['']\">Home <span class=\"sr-only\">(current)</span></a>\r\n      </li>\r\n      \r\n    </ul>\r\n    <span class=\"user-info\">\r\n      <span style=\"border-right: 2px solid black; padding-right: 1rem\">Logged in as : <strong>{{user.User_Name}}</strong></span>\r\n      <span style=\"padding-left: 1rem; cursor:pointer\" (click)=\"signOut()\"><span class=\"fa fa-fw fa-sign-out\"></span> Sign-Out</span>\r\n    </span>\r\n  </div>\r\n</nav>\r\n"

/***/ }),

/***/ "./src/app/topbar/topbar.component.less":
/*!**********************************************!*\
  !*** ./src/app/topbar/topbar.component.less ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/topbar/topbar.component.ts":
/*!********************************************!*\
  !*** ./src/app/topbar/topbar.component.ts ***!
  \********************************************/
/*! exports provided: TopbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TopbarComponent", function() { return TopbarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TopbarComponent = /** @class */ (function () {
    function TopbarComponent(authService) {
        this.authService = authService;
        this.user = this.authService.getLoggedInUser();
    }
    TopbarComponent.prototype.ngOnInit = function () {
    };
    TopbarComponent.prototype.signOut = function () {
        this.authService.logout();
    };
    TopbarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-topbar',
            template: __webpack_require__(/*! ./topbar.component.html */ "./src/app/topbar/topbar.component.html"),
            styles: [__webpack_require__(/*! ./topbar.component.less */ "./src/app/topbar/topbar.component.less")]
        }),
        __metadata("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], TopbarComponent);
    return TopbarComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    // baseUrl: 'http://localhost:80/'
    baseUrl: 'https://jjsmfgkpi-api.azurewebsites.net/'
    // baseUrl: 'http://b6c989d5.ngrok.io/'
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\miracleProjects\JSS\jss-front\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map